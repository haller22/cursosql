/* Modelagem basica */

CLIENTE

NOME CARACTER (30)
CPF CARACTER (30)
EMAIL CARACTER (30)
TELEFONE CARACTER (30)
SEXO CARACTER (30)
ENDERECO CARACTER (30)


/* PROCESSOS DE MODELAGEM */

/* ADM DE DADOS */

MODELAGEM CONCEITUAL - RASCUNHO 01

MODELAGEM LOGICA - PROGRAMAS COMO BRMODELO

/* AD - DBA */

MODELAGEM FISICA - SCRIPT DO BANCO


/* CRIANGO UM BANCO */

/* CONECTANDO */

USE PROJETO;

/* CRIANDO A TABELA */


CREATE TABLE CLIENTE (

	NOME VARCHAR (30),
	SEXO CHAR (1),
	EMAIL VARCHAR (30),
	CPF INT (11),
	TELEFONE VARCHAR (30),
	ENDERECO VARCHAR (30)
);


/*VERIFICANDO AS TABELAS*/

SHOW TABLES;

/* DESCREVENDO TABELAS */

DESC CLIENTE;

/* A 06 */

/*Tipos de dados */
/*
Todos os banos de dados possuem tipos que devem ser atribuidos aos dados de uma tabela.
Para caracteres literais, temos char e varchar, para números temos int e float, para
objetos como fotos e documentos, temos o blob, para textos extensos, temos o text. 
A disciplina de banco de dados é tão fantástica que ao entendermos o porque das coisas,
podemos iniciar já em modo avançado e um bom exemplo disso são os tipos. Há uma profissão
dentro da área que é a do analista de performance ou tuning, esse profissional é responsável
por analisar o banco de dados e deixá-lo mais rápido. Parece algo avançado, e é! Porém,
com alguas atitudes simples, podemos deixar o banco sem a necessidade de atuação desse profissional.

Cada caracter no banco de dados, vale 1 byte. Sendo assim, se eu entro com o dado JOÃO,
estou entrando com 4 bytes no meu banco. E o que isso tem a ver com a tiagem de tabelas?

O banco de dados funciona como um download de dados da internet. Se baixamos um arquivo de 1 giga,
temos um temo maior que o download de 50 megas, considerando a mesma velocidade de conexão.

Ao tiparmos uma tabela de modo errado ou displicente, vamos aumentar a quantidade de dados que 
será baixada do banco de dados, prolongando assim o tempo de resposta.

Uma comparacao bem didatica é o tipo char e varchar
A palavra var, vem de variant, em ingles, ou seja, que é dinâmica. Logo, vimos que 1 caracter
é igual a 1 byte. Vejamos então a tipagem

varchar(10)
char(10)

entrando a palavra joao

total de bytes varchar(10) = 4 bytes
toal de bytes cahr(10) = 10 bytes

isso ocorre pois o char não varia. Os caracteres restantes serao preenchidos com espaço. 
eles nao ficam vazios. Enquanto que no varchar, o tipo varia conforme o dado.
Entao utilizo sempre o varchar? Não. O charé ligeiramente mais performatico, por nao
ter que gastar tempo variando de tamanho. Entao a regra é utilizar sempre o char quando
sabemos que o numero de caracteres naquela coluna nao vai variar nunca. Por exemplo,
unidade federativa, com dois digitos, sexo com um digito e assim por diante. Vista a diferença
que podemos fazer com uma tipagem correta de tabelas, na próxima aula detalharemos os tipos do mysql
e nos modulos específicos de cada banco, você entenderá os tipos correspondentes no sql server
e no oracle, que mudam muito pouco.



*/

DELIMITER $

SELECT 10 + 10 AS "SOMA"$

CREATE PROCEDURE CONTA ( NUM1 INT, NUM2 INT ) 
BEGIN
	SELECT NUM1 + NUM2 AS "SOMA";
END
$

CALL CONTA (66,33)
$

DROP PROCEDURE CONTA
$


CREATE TABLE CURSOS (
	ID INT PRIMARY KEY AUTO_INCREMENT,
	NOME VARCHAR (30) NOT NULL,
	HORAS INT (3) NOT NULL,
	VALOR FLOAT (10,2) NOT NULL
)
$

SELECT * FROM CURSOS
$


CREATE PROCEDURE CAD_CURSO ( P_NOME VARCHAR(30), 
							P_HORAS INT (3), 
							P_VALOR FLOAT (10,2) )
BEGIN
	INSERT INTO CURSOS VALUES ( NULL, P_NOME, P_HORAS, P_VALOR );
END
$

CALL CAD_CURSO ("ELISEU", 33, 300000.00)
$

CREATE PROCEDURE SELL_CURSOS ()
BEGIN
	SELECT CURSOS.NOME, CURSOS.HORAS, CURSOS.VALOR
	FROM CURSOS;
END
$

CALL SELL_CURSOS ()
$


CREATE TABLE VENDEDORES (

	ID INT AUTO_INCREMENT PRIMARY KEY,
	NOME VARCHAR (30),
	SEXO CHAR (1),
	JANEIRO FLOAT (10,2),
	FEVEREIRO FLOAT (10,2),
	MARCO FLOAT (10,2)
)
$



CREATE PROCEDURE CAD_VENDEDOR ( P_NOME VARCHAR(30), 
							P_SEXO CHAR (1), 
							P_JANEIRO FLOAT (10,2),
							P_FEVEREIRO FLOAT (10,2),
							P_MARCO FLOAT (10,2) 
	)
BEGIN
	INSERT INTO VENDEDORES VALUES ( NULL, 
									P_NOME, 
									P_SEXO, 
									P_JANEIRO, 
									P_FEVEREIRO, 
									P_MARCO
	);
END
$


CALL CAD_VENDEDOR ( "ANA", "F", 654654.55, 584532.55, 153213.55 )
$CALL CAD_VENDEDOR ( "FELIPE", "M", 125321.55, 1212121.55, 55555555.55 )
$CALL CAD_VENDEDOR ( "JEAN", "M", 1232123.55, 11111.55, 3333333.55 )
$CALL CAD_VENDEDOR ( "CARLA", "F", 11111.55, 5000.55, 66666.55 )
$CALL CAD_VENDEDOR ( "JENY", "F", 654654.55, 65584532.55, 153213.55 )
$CALL CAD_VENDEDOR ( "TONY", "M", 999999.55, 5555555.55, 111111.55 )
$CALL CAD_VENDEDOR ( "FLA", "M", 656565.55, 626262.55, 616161.55 )
$CALL CAD_VENDEDOR ( "BIA", "F", 3333.55, 1111254.55, 45115151.55 )
$CALL CAD_VENDEDOR ( "ANA", "F", 654654.55, 65584532.55, 153213.55 )
$CALL CAD_VENDEDOR ( "DENISE", "F", 1654987.55, 12232323.55, 2323.00 )
$


/* MAX TRAS VALOR MAXIMO DE UMA COLUNA! */

SELECT MAX(FEVEREIRO) AS MAIOR_FEV
FROM VENDEDORES
$

/* MIN TRAS VALOR MINIMO DE UMA COLUNA! */

SELECT MIN(FEVEREIRO) AS MIN_FEV
FROM VENDEDORES
$

/* AVG TRAS VALOR MEDIO DE UMA COLUNA! */

SELECT AVG(FEVEREIRO) AS AVG
FROM VENDEDORES
$


/* VARIAS FUNCOES */

SELECT MAX(JANEIRO) AS MAX_JAN,
	   MIN(JANEIRO) AS MIN_JAN,
	   AVG(JANEIRO) AS MEDIA_JAN
FROM VENDEDORES
$


/* TRUNCATE FUNCOES */

SELECT MAX(JANEIRO) AS MAX_JAN,
	   MIN(JANEIRO) AS MIN_JAN,
	   TRUNCATE(AVG(JANEIRO),2) AS MEDIA_JAN
FROM VENDEDORES
$


/* Agregando com SUM */


SELECT SUM(JANEIRO) AS TOTAL_JAN
FROM VENDEDORES
$


/* VENDAS POR SEXO */

SELECT SEXO, 
	   SUM( JANEIRO ) AS TOTAL_JAN, 
	   SUM( FEVEREIRO ) AS TOTAL_FEV,
	   SUM( MARCO ) AS TOTAL_MAR
FROM VENDEDORES
GROUP BY SEXO

/* VENDEDOR QUE VENDEU MENOS EM MARCO E O SEU NOME */

/* ERRADO! */
SELECT NOME, 
	   MIN( MARCO ) AS MENOR_VAL
FROM VENDEDORES
$


SELECT * FROM VENDEDORES
$

SELECT MIN(MARCO) FROM VENDEDORES
$

/* SUBSELECT */

/*CERTOOO*/
SELECT NOME, MARCO FROM VENDEDORES
WHERE MARCO = (SELECT MIN(MARCO) FROM VENDEDORES)
$


/* NOME VALOR DE QUEM VENDEU MAIS */

SELECT NOME, MARCO FROM VENDEDORES
WHERE MARCO = (SELECT MAX(MARCO) FROM VENDEDORES)
$


/* NOME VALOR DE QUEM VENDEU MAIS */

SELECT NOME, MARCO FROM VENDEDORES
WHERE MARCO > (SELECT AVG(MARCO) FROM VENDEDORES)
$


SELECT NOME, MARCO FROM VENDEDORES
WHERE MARCO < (SELECT AVG(MARCO) FROM VENDEDORES)
$



/* Operações aritmeticas Colunas */

SELECT NOME,
	   JANEIRO,
	   FEVEREIRO,
	   MARCO,
	   TRUNCATE((JANEIRO+FEVEREIRO+MARCO),2) AS "TOTAL",
	   TRUNCATE((JANEIRO+FEVEREIRO+MARCO) * 0.25,2) AS "DESCONTO",
	   TRUNCATE((JANEIRO+FEVEREIRO+MARCO)/3,2) AS "MEDIA"
FROM VENDEDORES
$


/* Alterando Tabelas */

CREATE TABLE TABELA (
	COLUNA1 VARCHAR(30),
	COLUNA2 VARCHAR(30),
	COLUNA3 VARCHAR(30)
)
$

/** ADICIONANDO COLUNA SEM POSSICAO, ULTIMA POSICAO */

ALTER TABLE TABELA 
ADD PRIMARY KEY (COLUNA1)
$

ALTER TABLE TABELA
ADD COLUNA VARCHAR(30) NOT NULL
$

/* ADICIONANDO COLUNA COM POSICAO */

ALTER TABLE TABELA
ADD COLUNA100 INT(100)
AFTER COLUNA2
$


ALTER TABLE TABELA
ADD COLUNA4 VARCHAR(30)
AFTER COLUNA3
$


DESC TABELA
$

/* MODIFICAR TIPO DO CAMPO! */

ALTER TABLE TABELA
MODIFY COLUNA2 DATE NOT NULL
$

/* AJUDA! */
/* COMENTARIOS MYSQL */

HELP ALTER
$

HELP ALTER TABLE
$

/** RENOMEIA O NOME DA TABELA */

ALTER TABLE TABELA
RENAME PESSOA
$


/* CRIAR TABELA TIME */

CREATE TABLE TIME (

	IDTIME INT PRIMARY KEY AUTO_INCREMENT,
	TIME VARCHAR(30),
	ID_PESSOA VARCHAR(30)
)
$

ALTER TABLE TIME
ADD FOREIGN KEY(ID_PESSOA)
PREFERENCES PESSOA(COLUNA1)
$


SHOW CREATE TABLE TIME
$

/* ORGANIZAÇÃO DE CHAVES - CONSTRAINT (REGRA) */


CREATE TABLE JOGADOR (

	IDJOGADOR INT PRIMARY KEY AUTO_INCREMENT,
	NOME VARCHAR(30)
)
$

CREATE TABLE TIMES (

	IDTIME INT PRIMARY KEY AUTO_INCREMENT,
	NOMETIME VARCHAR(30),
	ID_JOGADOR INT,
	FOREIGN KEY (ID_JOGADOR)
	REFERENCES JOGADOR(IDJOGADOR)
)
$

INSERT INTO JOGADOR VALUES(NULL, 'GUERREIRO')
$INSERT INTO JOGADOR VALUES(NULL, 'FLAMENGO')
$


/* ORGANIZANDO CHAVES E ACOES DE CONSTRAINTS */


SHOW TABLES
$

DROP TABLE ENDERECO
$DROP TABLE TELEFONE
$DROP TABLE CLIENTE
$


CREATE TABLE CLIENTE (

	IDCLIENTE INT,
	NOME VARCHAR(30) NOT NULL
)
$

CREATE TABLE TELEFONE (

	IDTELEFONE INT,
	TIPO CHAR(3) NOT NULL,
	NUMERO VARCHAR(10) NOT NULL,
	ID_CLIENTE INT
)
$

ALTER TABLE CLIENTE ADD CONSTRAINT PK_CLIENTE
PRIMARY KEY(IDCLIENTE)
$


ALTER TABLE TELEFONE ADD CONSTRAINT FK_CLIENTE_TELEFONE
FOREIGN KEY(ID_CLIENTE) REFERENCES CLIENTE(IDCLIENTE)
$


SHOW TABLES
$

DESC TABLE_CONSTRAINTS
$

SELECT CONSTRAINT_SCHEMA AS "BANCO",
	   TABLE_NAME AS "TABELA",
	   CONSTRAINT_TYPE AS "TIPO"
FROM TABLE_CONSTRAINTS
$


SELECT CONSTRAINT_SCHEMA AS "BANCO",
	   CONSTRAINT_NAME AS "NOME REGRA",
	   TABLE_NAME AS "TABELA",
	   CONSTRAINT_TYPE AS "TIPO"
FROM TABLE_CONSTRAINTS
WHERE CONSTRAINT_SCHEMA = "COMERCIO"
$

ALTER TABLE TELEFONE
DROP FOREIGN KEY FK_CLIENTE_TELEFONE
$


ALTER TABLE TELEFONE ADD CONSTRAINT FK_CLIENTE_TELEFONE
FOREIGN KEY(ID_CLIENTE) REFERENCES CLIENTE(IDCLIENTE)
$


/** INTRODUÇÃO AS FANTASTICAS TRIGGERS */


CREATE TRIGGER NOME
BEFORE/AFTER INSERT/DELETE/UPDATE ON TABELA
FOR EACH ROW (PARA CADA LINHA)
BEGIN -> INICIO

	QUALQUER COMANDO SQL

END -> FIM


CREATE DATABASE AULA40
$

USE AULA40
$

CREATE TABLE USUARIO (

	IDUSUARIO INT PRIMARY KEY AUTO_INCREMENT,
	NOME VARCHAR(30),
	LOGIN VARCHAR(30),
	SENHA VARCHAR(100)
)
$


CREATE TABLE BKP_USUARIO (

	IDBACKUP INT PRIMARY KEY AUTO_INCREMENT,
	IDUSUARIO INT,
	NOME VARCHAR(30),
	LOGIN VARCHAR(30)
)
$

/** CRIANDO A TRIGGER */

DELIMITER $

CREATE TRIGGER BACKUP_USUARIO
BEFORE DELETE ON USUARIO
FOR EACH ROW
BEGIN

	INSERT INTO BKP_USUARIO VALUES(NULL, OLD.IDUSUARIO, OLD.NOME, OLD.LOGIN);
END
$


CREATE PROCEDURE CAD_USUARIO ( P_NOME VARCHAR(30), 
							P_LOGIN VARCHAR (30),
							P_SENHA VARCHAR (100) 
	)
BEGIN
	INSERT INTO USUARIO VALUES ( NULL, 
									P_NOME, 
									P_LOGIN,
									P_SENHA
	);
END
$


CALL CAD_USUARIO ( "ELISE", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "AAAHA", "ABF", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "ELISE", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "LEIA", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "CAMILA", "ASDFA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "JANAINA", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "SARA", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "CAMILA", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "LUCIANA", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "LUCILA", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "GLEIDIANE", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "JAMILE", "AAA", SHA("ASDFASDFASD") )
$CALL CAD_USUARIO ( "VALERIA", "AAA", SHA("ASDFASDFASD") )
$


DELETE FROM USUARIO WHERE IDUSUARIO='5'
$

SELECT * FROM BKP_USUARIO
$



